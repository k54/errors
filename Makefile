cover:
	@go test -coverprofile /tmp/gocover.out  .
	@go tool cover -func /tmp/gocover.out | { grep -v "100.0%" || :; } 
	@$(RM) /tmp/gocover.out
