# errors [![Go Reference](https://pkg.go.dev/badge/gitlab.com/k54/errors.svg)](https://pkg.go.dev/gitlab.com/k54/errors) [![Report card](https://goreportcard.com/badge/gitlab.com/k54/errors)](https://goreportcard.com/report/gitlab.com/k54/errors) [![Sourcegraph](https://sourcegraph.com/gitlab.com/k54/errors/-/badge.svg)](https://sourcegraph.com/gitlab.com/k54/errors?badge)

A simple error management library for go.

It implements the same basic interface as the standard `"errors"` library for interop, and more useful and composable features.

Also adds a simple `AddStack` function for easier debugging.

This library was made with performance in mind, and even traditionally expensive functions are fast and lazy, so if you don't print the error it is even faster.
