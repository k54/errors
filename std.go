package errors

import vanilla "errors"

// PART OF STD ERRORS PACKAGE
// Are all inlined, no penalty for the convenience

// As is a proxy to std errors.As, provided for ease of transition.
func As(err error, target interface{}) bool { return vanilla.As(err, target) }

// Is is a proxy to std errors.Is, provided for ease of transition.
func Is(err, target error) bool { return vanilla.Is(err, target) }

// New returns an error that formats as the given text
// Each call to New returns a distinct error value even if the text is identical.
func New(text string) error {
	return vanilla.New(text) //nolint:goerr113 // just a stdlib shortcut
}

// Unwrap returns the result of calling the Unwrap method on err,
// if err's type contains an Unwrap method returning error.
//
// Otherwise, Unwrap returns nil.
func Unwrap(err error) error { return vanilla.Unwrap(err) }
