package errors_test

import (
	"io"
	"testing"

	"gitlab.com/k54/errors"
)

func BenchmarkMultiString(b *testing.B) {
	b.ReportAllocs()
	err := &errors.Multi{Errors: []error{
		errors.New("lorem"),
		errors.New("ipsum"),
		io.EOF,
	}}
	for i := 0; i < b.N; i++ {
		_ = err.Error()
	}
}

func BenchmarkAddStack(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_ = errors.AddStack(io.EOF)
	}
}

func BenchmarkAddStack20(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_ = addStackMany(20)
	}
}

func addStackMany(depth int) error {
	if depth <= 1 {
		return errors.AddStack(io.EOF)
	}
	return addStackMany(depth - 1)
}

func BenchmarkWrap(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_ = errors.Wrap(io.EOF, "errors.BenchmarkWrap")
	}
}

func BenchmarkAutoWrap(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		_ = errors.AutoWrap(io.EOF)
	}
}
