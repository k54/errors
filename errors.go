// Package errors implements functions for handling errors
// It implements the same functions as the standard "errors" package,
// with a few added ones for ease of use
//
// Except where noted, functions that take a nil as their error will retrun nil,
// and do so very cheaply, so that you can use potentially expensive calls in main path.
//
// See godoc.org/errors for in-depth details about Is, As, Unwrap and New
package errors

import (
	"fmt"
	"runtime"
	"strings"
	"sync"
)

// AddStack wraps err to add a stack trace
//
// Quite a bit more expensive call compared to Wrap, but extremely useful,
// and run time should still be around the microsecond (few nanoseconds for Wrap)
//
// If err is nil, returns nil
//
// If call stack is extremely deep (10k+), has very poor performance
// (but you shouldn't record call stack in recursive functions anyway).
func AddStack(err error) error {
	if err == nil {
		return nil
	}
	// do not under-allocate, Callers call is expensive
	// but do not over-allocate, GC pressure is also expensive (still way less than Callers call)
	// 64 seems to be the best with benchmarks showing performance is not too bad for shallow stacks
	// (~10 depth) but still quite good for deep stacks (30+)
	const frames = 64
	rpc := make([]uintptr, frames)
	// 1 for Callers semantics, +1 for skipping AddStack
	const skipFrames = 2
	n := runtime.Callers(skipFrames, rpc)
	rpc = rpc[:n]

	// take more frames if needed, is not performant but correct
	if n == frames {
		// double frames read each iteration to attain correct value faster
		tframes := 64
		for n == tframes {
			tframes *= 2
			tmp := make([]uintptr, tframes)
			n = runtime.Callers(skipFrames+len(rpc), tmp)
			rpc = append(rpc, tmp[:n]...) //nolint:makezero // I know, that's the point
		}
	}
	return &Error{
		Err:   err,
		Stack: rpc,
	}
}

// Wrap returns an error that wraps err
//
// Additionally, it adds msg to the Error() message.
//
// If err is nil, returns nil.
func Wrap(err error, msg string) error {
	if err == nil {
		return nil
	}
	return &Error{Message: msg, Err: err}
}

// Wrapf returns an error that wraps err
//
// Additionally, it adds fmt.Sprintf(msg, a...) To the Error() message.
//
// If err is nil, returns nil.
func Wrapf(err error, msg string, a ...interface{}) error {
	if err == nil {
		return nil
	}
	return &Error{Message: fmt.Sprintf(msg, a...), Err: err}
}

var autoWrapCache sync.Map

// AutoWrap returns an error that wraps err
//
// Additionally, it adds the name of the caller function (and package)
// to the message
//
// Quite slower than manual Wrap("package.Func", err).
func AutoWrap(err error) error {
	if err == nil {
		return nil
	}
	rpc := make([]uintptr, 1)
	// result < 1 should NEVER happen, at worst it will be a runtime frame
	runtime.Callers(1+1, rpc) // 1 for Callers semantics, 1 for skipping AutoWrap

	pc := rpc[0]
	var fname string
	fnameI, ok := autoWrapCache.Load(pc)
	// get cached value
	if ok {
		fname, ok = fnameI.(string)
	}
	// cache miss
	if !ok {
		frame, _ := runtime.CallersFrames(rpc).Next()
		fname = frame.Function

		// +1 means at worst i will be 0, so never panics
		i := strings.LastIndexByte(fname, '/') + 1
		fname = fname[i:]
		autoWrapCache.Store(pc, fname)
	}
	return Wrap(err, fname)
}
