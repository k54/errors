package errors

import "fmt"

// CallInto appends fn() into err
//
// Useful for defer :
// defer errors.CallInto(&err, file.Close).
func CallInto(err *error, fn func() error) {
	*err = Append(*err, fn())
}

// AppendInto appends errs into err,
// useful for repetitive appends.
func AppendInto(err *error, errs ...error) {
	*err = Append(*err, errs...)
}

// RecoverInto tries to recover, and appends a representation of the error to err.
// If the recovered value is not an error, will create a new error based on fmt.Sprint(value)
//
// Use like: defer RecoverInto(&err).
func RecoverInto(err *error) {
	if e := recover(); e != nil {
		switch v := e.(type) {
		case error:
			AppendInto(err, v)
		case string:
			AppendInto(err, New(v))
		default:
			AppendInto(err, New(fmt.Sprint(v)))
		}
	}
}

// Append returns a *Multi (or nil) containing all errors
//
// Use like you would append:
// err = Append(err, myerror)
//
// if err is already a *Multi, appends to it
// otherwise, create a *Multi with err as first error.
func Append(err error, others ...error) error {
	// very simple cases
	if err == nil && len(others) == 0 {
		return nil
	}
	if len(others) == 0 {
		return err
	}

	var errs *Multi
	if err != nil {
		//nolint:errorlint // errors package, i know what I'm doing
		if merr, ok := err.(*Multi); ok {
			errs = merr
		} else {
			errs = new(Multi)
			errs.Errors = append(errs.Errors, err)
		}
	} else {
		errs = new(Multi)
	}

	for _, err = range others {
		if err == nil {
			continue
		}
		errs.Errors = append(errs.Errors, err)
	}

	if len(errs.Errors) == 0 {
		return nil
	}

	if len(errs.Errors) == 1 {
		return errs.Errors[0]
	}

	return errs
}
