package errors

import (
	"encoding/json"
	"fmt"
	"runtime"
	"strconv"

	"github.com/valyala/bytebufferpool"
)

// Error represents a simple error and its message.
//
// It may also optionally hold a wrapped error, and/or a stack trace.
type Error struct {
	Message string
	Err     error
	Stack   []uintptr
}

func (e *Error) Unwrap() error {
	return e.Err
}

func (e *Error) MarshalJSON() ([]byte, error) {
	var out struct {
		Message string        `json:"message"`
		Wrapped []interface{} `json:"wrapped,omitempty"`
		Stack   []string      `json:"stack,omitempty"`
	}
	out.Message = e.Message
	wr := e.Err
	for wr != nil {
		var wre *Error
		if As(wr, &wre) {
			out.Wrapped = append(out.Wrapped, wr)
		} else {
			out.Wrapped = append(out.Wrapped, map[string]interface{}{"message": wr.Error()})
		}
		wr = Unwrap(wr)
	}
	if len(e.Stack) > 0 {
		frames := runtime.CallersFrames(e.Stack)
		for {
			frame, more := frames.Next()
			if !more { // frame is valid here, but it's a runtime frame, so we don't want it
				break
			}
			out.Stack = append(out.Stack, fmt.Sprintf("%s:%d", frame.File, frame.Line))
		}
	}
	return json.Marshal(out)
}

func (e *Error) Error() string {
	buf := bytebufferpool.Get()
	defer bytebufferpool.Put(buf)
	buf.B = append(buf.B, e.Message...)
	if e.Err != nil {
		buf.B = append(buf.B, ": "...)
		buf.B = append(buf.B, e.Err.Error()...)
	}

	if len(e.Stack) > 0 {
		buf.B = append(buf.B, "("...)
		buf.B = strconv.AppendInt(buf.B, int64(len(e.Stack)), 10)
		buf.B = append(buf.B, " stack frames)"...)
	}
	return buf.String()
}

func (e *Error) Is(err error) bool {
	//nolint:errorlint // i know
	if er, ok := err.(*Error); ok {
		return er.Message == e.Message
	}
	return false
}

// Multi is an error that is actually composed of multiple errors
// An empty *Multi makes no sense, and should be nil instead.
type Multi struct {
	Errors []error `json:"errors"`
}

// Is implements errors.Is by comparing the first error directly.
func (m *Multi) Is(target error) bool {
	if m == nil {
		return false
	}
	for _, err := range m.Errors {
		if Is(err, target) {
			return true
		}
	}
	return false
}

// As implements errors.As by attempting to map to the first error.
func (m *Multi) As(target interface{}) bool {
	for _, err := range m.Errors {
		if As(err, target) {
			return true
		}
	}
	return false
}

func (m *Multi) Error() string {
	if m == nil {
		return ""
	}
	if len(m.Errors) == 0 {
		return ""
	}
	if len(m.Errors) == 1 {
		return "*errors.Multi[1]{" + m.Errors[0].Error() + "}"
	}
	// 32 would still reallocate every time
	// 64 should be fine sometimes, but 128 to be safe (64 more bytes on stack are not a big deal)
	// out should stay on stack, only string(out) should allocate to heap
	out := make([]byte, 0, 128)
	out = append(out, "*errors.Multi["...)
	out = strconv.AppendInt(out, int64(len(m.Errors)), 10)
	out = append(out, "]{"...)
	for _, err := range m.Errors {
		out = append(out, err.Error()...)
		out = append(out, ","...)
	}
	out = append(out, '}')

	return string(out)
}
