//nolint:ifshort,errorlint,goerr113 // better for tests
package errors_test

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"testing"

	"gitlab.com/k54/errors"
)

func TestMultiIs(t *testing.T) {
	var err error

	err = errors.Append(err, io.EOF)
	err = errors.Append(err, io.ErrShortWrite)
	if !errors.Is(err, io.EOF) {
		t.Fatal("error should wrap io.EOF")
	}
	var me *errors.Multi
	if !errors.Is(me, me) {
		t.Fatal("nil *Multi should Is() itself")
	}
	if errors.Is(me, io.EOF) {
		t.Fatal("nil *Multi should not Is() io.EOF")
	}
	if !errors.Is(err, err) {
		t.Fatal("*Multi should Is() itself")
	}
	if !errors.Is(err, io.ErrShortWrite) {
		t.Fatal("error should wrap io.ErrShortWrite")
	}
}

type httpError int

func (he httpError) Error() string {
	return http.StatusText(int(he))
}

func TestMultiAs(t *testing.T) {
	var err error
	var he httpError
	if errors.As(err, &he) {
		t.Fatal("nil error should not wrap an httpError")
	}

	me := new(errors.Multi)
	if errors.As(me, &he) {
		t.Fatal("nil error should not wrap an httpError")
	}
	err = errors.Append(err, httpError(404))
	err = errors.Append(err, io.ErrShortWrite)
	if !errors.As(err, &he) {
		t.Fatal("error should wrap an httpError")
	}
}

func TestUnwrap(t *testing.T) {
	// err := errors.Of("config", io.EOF)
	err := errors.Wrap(io.EOF, "config")
	t.Log(err)
	err = errors.Unwrap(err)
	t.Log(err)

	//nolint:errorlint // this is a test
	if err != io.EOF {
		t.Fatal("err should be io.EOF (last error), is", err)
	}
	err = errors.Unwrap(err)
	if err != nil {
		t.Fatal("Unwrap(io.EOF) should be nil", err)
	}
	err = errors.Unwrap(err)
	if err != nil {
		t.Fatal("Unwrap(nil) should be nil")
	}
}

func TestMultiNil(t *testing.T) {
	var err *errors.Multi
	if err.Error() != "" {
		t.Fatal("nil error returned a non empty string")
	}
	var me *errors.Multi = new(errors.Multi)
	if !errors.As(err, &me) {
		t.Fatal("nil error should As(nil)")
	}
	if !errors.Is(err, me) {
		t.Fatal("nil error should Is() other nil error")
	}
}

func TestAppendNil(t *testing.T) {
	var err error

	err = errors.Append(err, nil)
	err = errors.Append(err, nil, nil)
	if err != nil {
		t.Fatal("error was not nil")
	}
	err = errors.Append(nil, err)
	if err != nil {
		t.Fatal("error was not nil")
	}
	// t.Fail()
	// errors.New("test")
}

func TestAppendNothing(t *testing.T) {
	err := errors.New("test")
	err2 := errors.Append(err)

	if err != err2 {
		t.Fatal("error was change through Append(err)")
	}
	// t.Fail()
	// errors.New("test")
}

func TestAppends(t *testing.T) {
	var err error
	wrap := errors.Wrap(io.EOF, "hey")
	// wrap := errors.Of("hey", io.EOF)
	err = errors.Append(io.ErrShortWrite, nil)
	t.Log(err)
	err = errors.Append(err, nil, nil)
	t.Log(err)
	err = errors.Append(err, nil, wrap, nil)
	t.Log(err)
	err = errors.Append(err, errors.New("test"))
	t.Log(err)
	if !errors.Is(err, io.EOF) {
		t.Fatal("error should wrap io.EOF")
	}
	if errors.Is(err, errors.New("stuff")) {
		t.Fatal("error should not wrap new error")
	}
}

// func BenchmarkOf(b *testing.B) {
// 	b.ReportAllocs()
// 	for i := 0; i < b.N; i++ {
// 		err := errors.Of("test")
// 		errors.Ignore(err)
// 	}
// }

// func TestOfCall(t *testing.T) {
// 	var err error
// 	sentinel := errors.Of("hello")
// 	wrap := errors.Of("world", io.EOF, nil)
// 	_ = errors.Of("foo", nil)
// 	err = errors.Append(nil)
// 	err = errors.Append(err, sentinel)
// 	err = errors.Append(err, wrap)
// 	err = errors.Append(err, nil, nil)
// 	err = errors.Append(err, nil)

// 	if !errors.Is(err, io.EOF) {
// 		t.Fatal("error should wrap io.EOF")
// 	}
// 	if !errors.Is(err, sentinel) {
// 		t.Fatal("error should wrap sentinel")
// 	}
// 	if !errors.Is(err, wrap) {
// 		t.Fatal("error should wrap wrap")
// 	}
// 	if !errors.Is(sentinel, sentinel) {
// 		t.Fatal("sentinel should equal sentinel")
// 	}
// }

func TestCompareNew(t *testing.T) {
	msg := "a simple error message"
	err1 := errors.New(msg)
	err2 := errors.New(msg)

	if err1 == err2 {
		t.Fatal("err1 == err2")
	}
	if errors.Is(err1, err2) {
		t.Fatal("errors.Is(err1, err2)")
	}
}

func TestWrapIs(t *testing.T) {
	err := errors.Wrap(io.EOF, "config")

	if !errors.Is(err, io.EOF) {
		t.Fatal("error should wrap io.EOF")
	}
}

func TestMultiError(t *testing.T) {
	var err error

	err = errors.Append(err, io.EOF)
	err = errors.Append(err, io.ErrShortWrite)
	t.Log(err)
}

func TestNewError(t *testing.T) {
	err := errors.New("config")
	if err.Error() != "config" {
		t.Fatal("should have returned `config`")
	}
}

func retNil() error {
	return nil
}

func retErr() error {
	return errors.New("hey")
}

func TestCallInto(t *testing.T) {
	var err error
	errors.CallInto(&err, retNil)
	if err != nil {
		t.Fatal("err != nil:", err)
	}

	errors.CallInto(&err, retErr)
	if err == nil {
		t.Fatal("err == nil")
	}
}

func TestAppendInto(t *testing.T) {
	var err error
	errors.AppendInto(&err, retNil())
	if err != nil {
		t.Fatal("err != nil:", err)
	}

	errors.AppendInto(&err, retErr())
	if err == nil {
		t.Fatal("err == nil")
	}
}

func TestRecoverIntoString(t *testing.T) {
	var err error
	defer func() {
		if err == nil {
			t.Fatal("err nil")
		}
	}()
	defer errors.RecoverInto(&err)
	panic("hey")
}

func TestRecoverIntoErr(t *testing.T) {
	var err error
	defer func() {
		if err == nil {
			t.Fatal("err nil")
		}
	}()
	defer errors.RecoverInto(&err)
	panic(retErr())
}

func TestRecoverIntoAny(t *testing.T) {
	var err error
	defer func() {
		if err == nil {
			t.Fatal("err nil")
		}
	}()
	defer errors.RecoverInto(&err)
	panic(3)
}

func TestRecoverIntoNil(t *testing.T) {
	var err error
	defer func() {
		if err != nil {
			t.Fatal("err != nil", err)
		}
	}()
	defer errors.RecoverInto(&err)
	// normal execution
}

func TestAutoWrap(t *testing.T) {
	err := errors.New("config")
	// do it twice, for cache miss and hit
	for i := 0; i < 2; i++ {
		err = errors.AutoWrap(err)
		if err == nil {
			t.Fatal("err is nil")
		}
		if !strings.Contains(err.Error(), "TestAutoWrap") {
			t.Fatal("err does not contain its caller name")
		}
		if !strings.Contains(err.Error(), "errors_test") {
			t.Fatal("err does not contain its caller' package name")
		}
	}
}

func TestWrap(t *testing.T) {
	err := errors.Wrap(io.EOF, "stuff")
	if !errors.Is(err, io.EOF) {
		t.Fatal("Wrap() did not correctly wrap")
	}
	err = errors.Wrap(nil, "stuff")
	if err != nil {
		t.Fatal("Wrap(nil) did not return nil")
	}
}

func TestWrapf(t *testing.T) {
	err := errors.Wrapf(io.EOF, "stuff")
	if !errors.Is(err, io.EOF) {
		t.Fatal("Wrapf() did not correctly wrap")
	}
	err = errors.Wrapf(nil, "stuff")
	if err != nil {
		t.Fatal("Wrapf(nil) did not return nil")
	}
}

func TestErrorMessage(t *testing.T) {
	err := &errors.Error{Message: "hello"}
	if err.Error() != "hello" {
		t.Fatal("simple *Error did not return only the Message")
	}
}

func TestErrorIs(t *testing.T) {
	err := &errors.Error{Message: "hello"}
	err2 := &errors.Error{Message: "hello"}
	err3 := &errors.Error{Message: "hi"}
	if !errors.Is(err, err2) {
		t.Fatal("two different *Error with the same Message did not compare correctly under Is()")
	}
	if errors.Is(err, err3) {
		t.Fatal("two different *Error with the same Message did not compare correctly under Is()")
	}
}

func TestMultiString(t *testing.T) {
	err := &errors.Multi{}
	if err.Error() != "" {
		t.Fatal("empty *Multi should have empty Error() message")
	}
	err = &errors.Multi{Errors: []error{errors.New("hello")}}
	if err.Error() == "hello" {
		t.Fatal("single-child *Multi should manifest itself in Error() message")
	}
}

func TestAddStack(t *testing.T) {
	err := addFrames(128)
	// err := errors.NewStack("test")

	err = errors.AddStack(err)
	t.Log(err)
	// js, err := json.Marshal(err)
	js, err := json.MarshalIndent(err, "", "  ")
	t.Log(string(js), err)
}

func addFrames(depth int) error {
	if depth <= 1 {
		return errors.AddStack(errors.New("test"))
	}
	return addFrames(depth - 1)
}
func must(err error) {
	if err != nil {
		panic(err)
	}
}
func TestNil(t *testing.T) {
	must(errors.AddStack(nil))
	must(errors.AutoWrap(nil))
	must(errors.Wrap(nil, "test"))
	must(errors.Append(nil))
}
